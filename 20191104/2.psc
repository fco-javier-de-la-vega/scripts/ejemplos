Algoritmo tresNumeros
	// introducir 3 numeros
	// calcular el numero mas grande de los 3
	numero1 <- 0
	numero2 <- 0
	numero3 <- 0
	
	Escribir 'introduce un numero'
	Leer numero1
	Escribir 'introduce un numero'
	Leer numero2
	Escribir 'introduce un numero'
	Leer numero3
	
	Si numero1>numero2 && numero1>numero3 Entonces
		Escribir 'el ',numero1,' es mayor'
	SiNo
		Si numero1==numero2 && numero1>numero3 Entonces
			Escribir 'son iguales'
		SiNo
			Si numero2>numero3 && numero2>numero1 Entonces
				Escribir 'el ',numero2,' es mayor'
			SiNo
				Escribir 'el ',numero3,' es mayor'
			FinSi
		FinSi
	FinSi
FinAlgoritmo

